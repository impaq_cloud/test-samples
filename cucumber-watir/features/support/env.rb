begin require 'rspec/expectations'; rescue LoadError; require 'spec/expectations'; end

require 'selenium-webdriver'
require 'watir-webdriver'

# "before all"

if ENV['LOCAL']
	webdriver = Selenium::WebDriver.for :firefox
else
  webdriver = Selenium::WebDriver.for :remote, :url => "http://176.34.249.236:4444/wd/hub", :desired_capabilities => :ie 
end

browser = Watir::Browser.new webdriver

Before do
  @browser = browser 
end

# "after all"
at_exit do
  browser.close
end
