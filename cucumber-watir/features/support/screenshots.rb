# This is an example of how you can set up screenshots for your
# browser testing. Just run cucumber with --format html --out report.html
#
# The code below will work on OS X or Windows (with IE Watir only).
# Adding support for other platforms should be easy - as long as there is a 
# ruby library or command line tool to take pictures.
#
module Screenshots
  
	def Screenshots.create_screenshot_dir(dir)
		FileUtils.mkdir(dir) unless File.directory? dir
	end

    def embed_screenshot(id)
      @browser.driver.save_screenshot id
			embed("#{id}", "image/png")
    end
end
World(Screenshots)

Screenshots.create_screenshot_dir "screenshot"

After do
  embed_screenshot("screenshot/screenshot-#{Time.new.to_i}.png")
end

# Other variants:
#
# Only take screenshot on failures
#
#   After do |scenario|
#     embed_screenshot("screenshot-#{Time.new.to_i}") if scenario.failed?
#   end
#
# Only take screenshot for scenarios or features tagged @screenshot
#
#   After(@screenshot) do
#     embed_screenshot("screenshot-#{Time.new.to_i}")
#   end
